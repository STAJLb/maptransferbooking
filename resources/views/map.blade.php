<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MapTransferBooking</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          type="text/css">


    <!-- Styles -->
    <style>
        @media (max-width:800px){
            .filter{
                height: 140px !important;
                 margin-left: 15px;
                 margin-right: 15px;
            }
        }
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Comic Sans MS', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        html, body {
            height: 100%;
            overflow: hidden;
        }

        #map {

            position: static !important;

        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        * {
            margin: 0;
            padding: 0;
        }

        #page-wrap {
            margin: auto 0;
        }

        .treeview {
            margin: 10px 0 0 20px;
        }

        ul {
            list-style: none;
        }

        .treeview li {
            background: url(http://jquery.bassistance.de/treeview/images/treeview-default-line.gif) 0 0 no-repeat;
            padding: 2px 0 2px 16px;
        }

        .treeview > li:first-child > label {
            /* style for the root element - IE8 supports :first-child
            but not :last-child ..... */

        }

        .treeview li.last {
            background-position: 0 -1766px;
        }

        .treeview li > input {
            height: 16px;
            width: 16px;
            /* hide the inputs but keep them in the layout with events (use opacity) */
            opacity: 0;
            filter: alpha(opacity=0); /* internet explorer */
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(opacity=0)"; /*IE8*/
        }

        .treeview li > label {
            -webkit-touch-callout: none; /* iOS Safari */
            -webkit-user-select: none;   /* Chrome/Safari/Opera */
            -khtml-user-select: none;    /* Konqueror */
            -moz-user-select: none;      /* Firefox */
            -ms-user-select: none;       /* Internet Explorer/Edge */
            user-select: none;
            background: url(https://www.thecssninja.com/demo/css_custom-forms/gr_custom-inputs.png) 0 -1px no-repeat;
            /* move left to cover the original checkbox area */
            margin-left: -20px;
            /* pad the text to make room for image */
            padding-left: 20px;
        }

        /* Unchecked styles */

        .treeview .custom-unchecked {
            background-position: 0 3px;
        }

        .treeview .custom-unchecked:hover {
            background-position: 0 -17px;
        }

        /* Checked styles */

        .treeview .custom-checked {
            background-position: 0 -77px;
        }

        .treeview .custom-checked:hover {
            background-position: 0 -97px;
        }

        /* Indeterminate styles */

        .treeview .custom-indeterminate {
            background-position: 0 -117px;
        }

        .treeview .custom-indeterminate:hover {
            background-position: 0 -117px;
        }


    </style>
</head>
<body>

<div style="height:100%" class="row">

    <div class="col-md-10  col-xs-12"  style="  min-height: 75%;max-height:100%">
        <div id="map"></div>
    </div>
    <div style="    background: radial-gradient(circle farthest-corner at 100px 50px, #FBF2EB, #7d7880e3);z-index: 999;height: 100vh;overflow-y: scroll; background-color: white" class="filter col-md-2 col-xs-12">
        <div class="text-center">Языковая панель</div>
        <select class="form-control form-control-sm">
            @foreach($languages as $language)
              <option value="{{$language->tag}}">{{$language->name}}</option>
           
            @endforeach
        </select>
        <ul  class="treeview">
            @foreach($categories as $category)
                <li class="last">
                    
                    <input checked class="checkbox" type="checkbox" name="checkbox" id="{{$category->id}}" value="{{$category->id}}">
                    <label for="{{$category->id}}" class="custom-checked"><img style="height: 17px;" src="{{asset($category->url_icon)}}" />  {{$category->name}} </label>
                    <ul>
                        @foreach($category->children as $children)
                            <li>
                                @php
                                    $i=0;
                                @endphp
                                <input  checked value="{{$children->id}}" class="checkbox" type="checkbox"
                                       id="{{$children->id}}-{{$i}}">
                                <label  for="{{$children->id}}-@php echo $i++;@endphp"
                                       class="custom-checked"><img style="height: 17px;" src="{{asset($children->url_icon)}}" /> {{$children->name}} </label>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>
       
    </div>

</div>
<!-- jQuery 2.2.3 -->
<script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/bootstrap/js/bootstrap.min.js"></script>


<script>
    var map;
    var infowindow;
    function initMap(categoriesCheckbox = []) {


        var latlng = {lng: 59.92290240000001, lat: 30.490755700000022};
        var mapOptions = {
            center: latlng,
            zoom: 3
        };
        var markersObjects = [];
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        infowindow = new google.maps.InfoWindow();

        var path = location.pathname.split('/');
        var locale = path[path.length-1];
        $("select").val(locale);
        if(!locale){
            locale = 'ru';
            $("select").val('ru');
        }
        $(':checkbox').each(function() {
            if(this.checked) {
                   categoriesCheckbox.push($(this).val());
                }
            });



        $.ajax({
            type: 'GET',
            url: '/markers/locale/' + locale,
            success: function (data) {
                var markers = data;
                console.log(markers);

                for (var i = 0; i < markers.length; i++) {
                    var markerObject = markers[i];
                    var image = {
                        url: document.location.origin + markerObject.url_icon,
                    };
                    if(markerObject.categories){
                        categoriesMarkerObject = markerObject.categories.split(',');
                        categoriesMarkerObject.forEach(function(item, i, arr) {
                            if(categoriesCheckbox.indexOf( item ) != -1){
                                markersObjects.push(createMarker(markerObject.latitude, markerObject.longitude,markerObject.description,markerObject.name,image));
                            }
                        });
                    }


                }
                var markerCluster = new MarkerClusterer(map, markersObjects,
                    {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
            },
            error: function () {
                console.log(data);
            }
        });

    }
    function createMarker(latitude,longitude ,content,title,image) {
        var marker = new google.maps.Marker({
            position: {lat: parseFloat(latitude), lng: parseFloat(longitude)},
            map: map,
            title: title,
            icon: image,
        });
        marker.addListener('click', function() {
            infowindow.setContent(content);
            infowindow.open(map, this);
        });

        return marker;
    }

    function getIds() {

        var searchIDs = $('input:checked').map(function () {
            return $(this).val();
        });
        console.log(searchIDs.get());
    }
</script>

<script>
    $(function () {

        $('input[type="checkbox"]').change(checkboxChanged);

        function checkboxChanged() {
            var $this = $(this),
                checked = $this.prop("checked"),
                container = $this.parent(),
                siblings = container.siblings();

            container.find('input[type="checkbox"]')
                .prop({
                    indeterminate: false,
                    checked: checked
                })
                .siblings('label')
                .removeClass('custom-checked custom-unchecked custom-indeterminate')
                .addClass(checked ? 'custom-checked' : 'custom-unchecked');

            checkSiblings(container, checked);
        }

        function checkSiblings($el, checked) {
            var parent = $el.parent().parent(),
                all = true,
                indeterminate = false;

            $el.siblings().each(function () {
                return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
            });

            if (all && checked) {
                parent.children('input[type="checkbox"]')
                    .prop({
                        indeterminate: false,
                        checked: checked
                    })
                    .siblings('label')
                    .removeClass('custom-checked custom-unchecked custom-indeterminate')
                    .addClass(checked ? 'custom-checked' : 'custom-unchecked');

                checkSiblings(parent, checked);
            } else if (all && !checked) {
                indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;

                parent.children('input[type="checkbox"]')
                    .prop("checked", checked)
                    .prop("indeterminate", indeterminate)
                    .siblings('label')
                    .removeClass('custom-checked custom-unchecked custom-indeterminate')
                    .addClass(indeterminate ? 'custom-indeterminate' : (checked ? 'custom-checked' : 'custom-unchecked'));

                checkSiblings(parent, checked);
            } else {
                $el.parents("li").children('input[type="checkbox"]')
                    .prop({
                        indeterminate: true,
                        checked: false
                    })
                    .siblings('label')
                    .removeClass('custom-checked custom-unchecked custom-indeterminate')
                    .addClass('custom-indeterminate');
            }
        }
    });
    $(":checkbox").change(function() {

        setTimeout(function() {
            var checkBoxValue = [];
            $(':checkbox').each(function() {
                if(this.checked) {
                    checkBoxValue.push($(this).val());
                }
                initMap(checkBoxValue);
            });
        }, 100);

    });

    $("select").change(function(){
        location.href = document.location.origin +'/locale/' + $(this).val();
        $("select").val($(this).val());
    });

</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvsTov8LX_O_jfK6TSgc037Us6QRXymcw&callback=initMap">
</script>
</body>
</html>

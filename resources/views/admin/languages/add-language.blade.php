@extends('admin.cover_admin')

@section('title')
    Форма создания языка
@endsection

@section('content')
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Форма создания языка</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive ">
                        <div class="modal fade">
                            <h4 class="modal-title" id="myModalLabel">Форма создания языка</h4>

                        </div>
                        <div class="modal-body">
                            @if( count($errors) > 0 )
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach( $errors->all() as $error )
                                            <li>{{ $error }}</li> @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form method="post" action="" class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Название языка</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name"
                                               placeholder="Название языка" value="{{old('name')}}">
                                        <p class="help-block">Пример:Русский язык.</p>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Тег языка</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="tag"
                                               placeholder="Название языка" value="{{old('tag')}}">
                                        <p class="help-block">Пример:ru,en.</p>
                                    </div>

                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" id="btn-save" value="add">Создать</button>
                            </form>


                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>


@endsection
@section('script')
    <script>

        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2().select2("val", null);
        })
    </script>

@endsection

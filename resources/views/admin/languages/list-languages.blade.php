@extends('admin.cover_admin')
@section('title')
    Список языков
@endsection
@section('content')
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Таблица языков</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive ">
                        @if(session('message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{--<h4><i class="icon fa fa-ban"></i>Успешно!</h4>--}}
                                {{ session('message') }}
                            </div>
                        @endif
                        <table id="example1" class="table table-bordered table-striped ">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Название языка</th>
                                <th>Тег</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($languages as $language)
                                <tr id="language_object_{{$language->id}}">
                                    <td>{{$language->id}}</td>
                                    <td>{{$language->name}}</td>
                                    <td>{{$language->tag}}</td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-xs delete-language" value="{{$language->id}}">Удалить язык</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody></table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>


@endsection
@section('script')
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
    <script src="{{asset('dist/js/languages.js?v=7')}}"></script>
@endsection
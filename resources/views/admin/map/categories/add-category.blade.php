@extends('admin.cover_admin')

@section('title')
    Форма создания категории
@endsection

@section('content')
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Форма создания категории</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive ">
                        <div class="modal fade">
                            <h4 class="modal-title" id="myModalLabel">Форма создания категории</h4>

                        </div>
                        <div class="modal-body">
                            @if( count($errors) > 0 )
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach( $errors->all() as $error )
                                            <li>{{ $error }}</li> @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form method="post" action="" class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Название категории</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name"
                                               placeholder="Название категории" value="{{old('name')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Родительская
                                        категория</label>
                                    <div class="col-sm-9">
                                        <select name="parent_category" class="form-control select2"
                                                data-placeholder="Выберите категории"
                                                style="width: 100%;">
                                            <option></option>
                                            @foreach($parentCategories as $parentCategory)
                                                <option value="{{$parentCategory->id}}">{{$parentCategory->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                  <div class="form-group">
                                    <label  class="col-sm-3 control-label">Иконка</label>
                                    <div class="col-sm-9">
                                        <div class="input-group-btn">
                                            <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary btn-block btn-flat">
                                                <i class="fa fa-picture-o"></i> Выбрать иконку
                                            </a>
                                        </div>
                                    <br>
                                    
                                    <input  id="thumbnail" class="form-control" type="hidden" name="filepath_icons">
                                        <img id="holder" style="margin-top:15px;max-height:100px;" >
                                        <span>Другие языки можно добавить через список категорий, нажав кнопку "Посмотреть информацию".</span>
                                    </div>
                                    
                                </div>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <button type="submit" class="btn btn-primary" id="btn-save" value="update">Сохранить</button>
                            </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>


@endsection
@section('script')
 <script src="/vendor/unisharp/laravel-filemanager/public/js/lfm.js"></script>
    <script>
        let options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
        $('#lfm').filemanager('image');
    </script>
    <script>
    
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2().select2("val", null);
        })
    </script>

@endsection

@extends('admin.cover_admin')
@section('title')
    Список категорий объектов
@endsection
@section('content')
    <style>
        td{
            vertical-align: middle !important;
        }
    </style>
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Таблица категорий объектов</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive ">
                        @if(session('message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{--<h4><i class="icon fa fa-ban"></i>Успешно!</h4>--}}
                                {{ session('message') }}
                            </div>
                        @endif
                        <table id="example1" class="table table-bordered table-striped ">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Название категории</th>
                                <th>Дочерние категории</th>
                                <th>Иконка</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)
                                    <tr id="category_object_{{$category->id}}">
                                        <td>{{$category->id}}</td>
                                        <td>{{$category->name}}</td>
                                        <td >
                                        @foreach($category->children as $children)
                                                <div style="cursor: no-drop;" class="btn btn-info btn-xs">{{$children->name}}</div>
                                        @endforeach
                                        </td>
                                        <td>
                                            <div class="user-block"><img class="img-circle img-bordered-sm" src="http://map.transferbooking.org{{$category->url_icon}}"></div>
                                        </td>   
                                        <td>
                                            <button type="button" class="btn btn-info btn-xs open-modal" value="{{$category->group_id}}">Посмотреть информацию</button>
                                            <button type="button" class="btn btn-danger btn-xs delete-category" value="{{$category->id}}">Удалить категорию</button>
                                        </td>
                                    </tr>
                                    @foreach ($category->children as $children)
                                        <tr id="category_object_{{$children->id}}">
                                            <td>{{$children->id}}</td>
                                            <td>{{$children->name}}</td>
                                            <td >
                                               Является дочерней категорией
                                            </td>
                                            <td>
                                            <div class="user-block"><img class="img-circle img-bordered-sm" src="{{asset($children->url_icon)}}"></div>
                                        </td>   
                                            <td>
                                                <button type="button" class="btn btn-info btn-xs open-modal" value="{{$children->group_id}}">Посмотреть информацию</button>
                                                <button type="button" class="btn btn-danger btn-xs delete-category" value="{{$children->id}}">Удалить категорию</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody></table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Форма редактирования категории</h4>
                </div>
                <div class="modal-body">
                    <b>Выберите язык</b>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">

                            @foreach($languages as $language)
                                <li class=""><a href="#{{$language->tag}}" data-toggle="tab" aria-expanded="false">{{$language->name}}</a></li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($languages as $language)
                                <div class="tab-pane" id="{{$language->tag}}">
                                    <form method="post" action="" class="form-horizontal" >
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Название категории</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="name_{{$language->tag}}"   name="name_{{$language->tag}}" placeholder="Название категории" >
                                            </div>
                                        </div>
                                        @if($language->tag === 'ru')
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Родительская категория</label>
                                            <div class="col-sm-9">
                                                <select id="parent_category_{{$language->tag}}" class="form-control select2" data-placeholder="Выберите категории"
                                                        style="width: 100%;">
                                                    @foreach($parentCategories as $parentCategory)
                                                        <option value="{{$parentCategory->id}}">{{$parentCategory->name}}</option>
                                                    @endforeach
                                                </select>
                                                <p class="help-block">Родительская категория относится ко всем переводам.</p>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 control-label">Иконка</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group-btn">
                                                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary btn-block btn-flat">
                                                            <i class="fa fa-picture-o"></i> Выбрать иконку
                                                        </a>
                                                    </div>
                                                    <input  id="thumbnail" class="form-control" type="hidden" name="filepath_icons"/>
                                                    <img id="holder" style="margin-top:15px;max-height:100px;" />
                                                </div>
                                            </div>

                                        @endif

                                    </form>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="button" class="btn-save btn btn-primary" value="{{$language->tag}}">Сохранить</button>
                                    <input type="hidden" class="category_group_id">
                                </div>
                        @endforeach
                        <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>


                </div>

            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="/vendor/unisharp/laravel-filemanager/public/js/lfm.js"></script>
    <script>
        
      
        $('#lfm').filemanager('image');
    </script>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
    <script src="{{asset('dist/js/categories.js?v=16')}}"></script>
@endsection
@extends('admin.cover_admin')

@section('title')
    Форма создания объекта
@endsection

@section('content')
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Форма создания объекта</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive ">
                        <div class="modal fade">
                            <h4 class="modal-title" id="myModalLabel">Форма редактирования вписки</h4>

                        </div>
                        <div class="modal-body">
                            @if( count($errors) > 0 )
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach( $errors->all() as $error )
                                            <li>{{ $error }}</li> @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form method="post" action="" class="form-horizontal">

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Название объекта</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="title_marker" value="{{ old('title_marker') }}" name="title_marker"
                                               placeholder="Название метки" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Описание объекта</label>
                                    <div class="col-sm-9">
                                        <textarea type="text" class="form-control"
                                                  name="description_marker" placeholder="Описание метки">{{ old('description_marker') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Координаты</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="text" value="{{ old('latitude') }}" class="form-control" name="latitude"
                                                   placeholder="Широта">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" value="{{ old('longitude') }}" class="form-control" name="longitude"
                                                   placeholder="Долгота" value="">
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label">Иконка</label>
                                    <div class="col-sm-9">
                                        <div class="input-group-btn">
                                            <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary btn-block btn-flat">
                                                <i class="fa fa-picture-o"></i> Выбрать иконку
                                            </a>
                                        </div>
                                    <br>
                                    <input  id="thumbnail" class="form-control" type="hidden"  value="{{ old('filepath_icons') }}" name="filepath_icons">
                                        <img id="holder" style="margin-top:15px;max-height:100px;">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Категории</label>
                                    <div class="col-sm-9">
                                        <select name="categories[]" class="form-control select2" multiple="multiple"
                                                data-placeholder="Выберите категории"
                                                style="width: 100%;">
                                            <option></option>
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>

                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" id="btn-save" value="add">Создать</button>
                            </form>

                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>


@endsection

@section('script')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-filemanager/public/js/lfm.js"></script>
    <script>
        let options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
        CKEDITOR.replace('description_marker', options);
        $('#lfm').filemanager('image');
    </script>
    <script>

        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
    </script>

@endsection
@extends('admin.cover_admin')
@section('title')
    Список объектов карты
@endsection
@section('content')
    <style>
        td {
            vertical-align: middle !important;
        }
    </style>
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Таблица объектов карты</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive ">
                        @if(session('message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                {{--<h4><i class="icon fa fa-ban"></i>Успешно!</h4>--}}
                                {{ session('message') }}
                            </div>
                        @endif
                        <table id="example1" class="table table-bordered table-striped ">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Название метки</th>
                                <th>Координаты</th>
                                <th>Категории</th>
                                <th>Иконка</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($markers as $marker)
                                <tr id="category_object_{{$marker->id}}">
                                    <td>{{$marker->id}}</td>
                                    <td>{{$marker->name}}</td>
                                    <td>{{$marker->latitude}} - {{$marker->longitude}}</td>
                                    <td>{{$marker->categories}}
                                        @php
                                            //if(!is_null($marker->categories)){

                                          //  $categoriesMarkers = explode(',',$marker->categories);

                                           // if($categoriesMarkers){
                                             //   foreach ($categoriesMarkers as $categoriesMarker){
                                            //        echo  $categories->where('id',$categoriesMarker)->first()->name.' ';
                                           //     }
                                         //   }
//
                                   //  }
                                        @endphp
                                    </td>
                                    <td>
                                        <div class="user-block"><img class="img-circle img-bordered-sm"
                                                                     src="{{asset($marker->url_icon)}}"/></div>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-xs open-modal"
                                                value="{{$marker->group_id}}">Посмотреть информацию
                                        </button>
                                        <button type="button" class="btn btn-danger btn-xs delete-marker"
                                                value="{{$marker->group_id}}">Удалить объект
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <p>No markers</p>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="    width: 80%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Форма редактирования события</h4>
                </div>
                <div class="modal-body">
                    <b>Выберите язык</b>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">

                            @foreach($languages as $language)
                                <li class=""><a href="#{{$language->tag}}" data-toggle="tab"
                                                aria-expanded="false">{{$language->name}}</a></li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($languages as $language)
                                <div class="tab-pane" id="{{$language->tag}}">
                                    <form method="post" action="" class="form-horizontal">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Название
                                                объекта</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control"
                                                       id="title_marker_{{$language->tag}}" name="title_marker"
                                                       placeholder="Название метки" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Описание
                                                объекта</label>
                                            <div class="col-sm-9">
                                        <textarea type="text" class="form-control"
                                                  id="description_marker_{{$language->tag}}"
                                                  name="description_marker" placeholder="Описание метки"></textarea>
                                            </div>
                                        </div>
                                        @if($language->tag == 'ru')
                                            <hr>
                                            <b>Общие поля:</b>
                                            <div class="form-group">
                                                <label for="inputEmail3"
                                                       class="col-sm-3 control-label">Координаты</label>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control"
                                                               id="latitude_{{$language->tag}}"
                                                               name="latitude_{{$language->tag}}"
                                                               placeholder="Широта">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control"
                                                               id="longitude_{{$language->tag}}"
                                                               name="longitude_{{$language->tag}}"
                                                               placeholder="Долгота" value="">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Иконка</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group-btn">
                                                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary btn-block btn-flat>
                                                <i class=" fa fa-picture-o"></i> Выбрать иконку
                                                        </a>
                                                    </div>
                                                    <br>
                                                    <input id="thumbnail" class="form-control" type="hidden"
                                                           name="filepath_icons">
                                                    <img id="holder" style="margin-top:15px;max-height:100px;">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputEmail3"
                                                       class="col-sm-3 control-label">Категории</label>
                                                <div class="col-sm-9">
                                                    <select id="categories" name="categories[]"
                                                            class="form-control select2" multiple="multiple"
                                                            data-placeholder="Поиск"
                                                            style="width: 100%;">
                                                        <option></option>
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endforeach

                                                    </select>
                                                </div>

                                            </div>
                                        @endif
                                    </form>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="button" class="btn-save btn btn-primary" value="{{$language->tag}}">
                                        Сохранить
                                    </button>
                                    <input type="hidden" class="marker_group_id">
                                </div>
                        @endforeach
                        <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>


                </div>

            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-filemanager/public/js/lfm.js"></script>
    <script>
        $('#lfm').filemanager('image');
    </script>
    <script>

        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
    </script>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
    <script src="{{asset('dist/js/markers.js?v=6')}}"></script>
@endsection
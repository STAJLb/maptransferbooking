$(document).ready(function () {

    var url = "/admin/markers";
    let options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };


    //display modal form for task editing
    $('body').on('click', '.open-modal', function () {
        $('#myModal').modal('show');
        var group_id = $(this).val();

        console.log(group_id);
        $.get(url + '/' + group_id, function (data) {
           // console.log(data);
            //success data
            for (var i = 0; i < data.length; i++) {
                var marker = data[i];
                $('.marker_group_id').val(marker.group_id);
                CKEDITOR.replace('description_marker_' + marker['lang'], options);
                CKEDITOR.instances['description_marker_'+ marker['lang']].setData(marker.description);
                //$('#description_marker_' + marker['lang']).html(marker.description);
                $('#title_marker_' + marker['lang']).val(marker.name);
                $('#latitude_' + marker['lang']).val(marker.latitude);
                $('#longitude_' + marker['lang']).val(marker.longitude);
                $('#thumbnail').val(marker.url_icon);
                var baseUrl = document.location.origin;
                $('#holder').attr('src', baseUrl + marker.url_icon);
                if(marker.categories != null && marker.lang == 'ru'){
                    var valuesCategories = new Array();
                    var categories = marker.categories.split(',');

                    categories.forEach(function (item,i,categories) {
                        valuesCategories.push(parseInt(item));
                    });
                    console.log(valuesCategories);

                    $("#categories").val(valuesCategories).trigger('change');
                }


            }
        })
    });


    $('body').on('click', '.delete-marker', function () {
        var marker_id = $(this).val();

        if (confirm("Подтвердите свое действие.")) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            var formData = {
                id: marker_id,
            }
            var type = "delete";
            var my_url = url + '/' + marker_id;
            $.ajax({

                type: type,
                url: my_url,
                data: formData,
                dataType: 'json',
                success: function (data) {

                    document.location.reload(true);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }


    });


    $(".btn-save").click(function (e) {
        var lang = $(this).val();
        if (confirm("Подтвердите свое действие.")) {
            saveData(e,lang);
        }
    });


    function formCategory(data) {
        var user = '<tr  id="category_object_' + data.id + '"><td>' + data.id + '</td><td>' + data.name + '</td><td>' + 'Дочерние категории' + '</td>';
        user += '<td class="text-center"><div class="btn-group">';
        user += '<button type="button" value="' + data.id + '" class="btn btn-default open-modal">Посмотреть информацию</button>';
        user += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">';
        user += '   <span class="caret"></span>';
        user += '   <span class="sr-only">Toggle Dropdown</span>';
        user += ' </button>';
        user += '<ul class="dropdown-menu" role="menu">';
        user += ' <li><button type="button" class="delete-category" value="' + data.id + '">Удалить вписку</button></li>';
        user += '</ul>';
        user += ' </div> </td></tr>';

        return user;

    }

    function saveData(e, lang) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        e.preventDefault();

        var formData = {
            lang: lang,
            name: $('#title_marker_' + lang).val(),
            description: CKEDITOR.instances['description_marker_'+ lang].getData(),
            latitude: $('#latitude_' + lang).val(),
            longitude: $('#longitude_' + lang).val(),
            group_id: $('.marker_group_id').val(),
            url_icon: $('#thumbnail').val(),
            categories: $('#categories').val(),
        }
        console.log(formData);
        var my_url = url;
        my_url += '/edit-marker';


        $.ajax({
            type: 'PUT',
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                console.log('Данные пришли: ', data);
                // if (state == "add") { //if user added a new record
                //     $('#tasks-list').append(user);
                // } else { //if user updated an existing record
                //
                //     $("#party_id" + data.id).replaceWith(formUser(data));
                // }

                document.location.reload(true);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }


});
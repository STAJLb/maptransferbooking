$(document).ready(function () {

    var url = "/admin/categories";
  



    //display modal form for task editing
    $('body').on('click', '.open-modal', function () {
        $('#myModal').modal('show');
        var group_id = $(this).val();
        console.log(group_id);
        $.get(url + '/' + group_id, function (data) {
            console.log(data);
            //success data
            for(var i=0;i<data.length;i++){
                var category = data[i];
               // console.log($('#name_'+category['lang']));
                //$('#category_id').val(data.id);
                $('#name_' + category['lang']).val(category.name);
                $('.category_group_id').val(category.group_id);
                if(category.parent_id == 0){
                    $('#parent_category_'+ category['lang']).attr('disabled',true);
                }else{
                    $('#parent_category_'+ category['lang']).attr('disabled',false);
                    $('#parent_category_'+ category['lang']).val(category.parent_id).trigger('change');
                }
                $('#thumbnail').val(category.url_icon);
                var baseUrl = document.location.origin;
            
                $('#holder').attr('src', baseUrl + category.url_icon);


            }
        })
    });



    $('body').on('click', '.delete-category', function () {
        var category_id = $(this).val();

        if (confirm("Подтвердите свое действие.Если категория была родительской, все дочерние категории будут удалены.")) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            var formData = {
                id: category_id,
            }
            var type = "delete";
            var my_url = url + '/' + category_id;
            $.ajax({

                type: type,
                url: my_url,
                data: formData,
                dataType: 'json',
                success: function (data) {

                    document.location.reload(true);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }


    });


    function formCategory(data) {
        var user = '<tr  id="category_object_' + data.id + '"><td>' + data.id + '</td><td>' + data.name + '</td><td>' + 'Дочерние категории' + '</td>';
        user += '<td class="text-center"><div class="btn-group">';
        user += '<button type="button" value="' + data.id + '" class="btn btn-default open-modal">Посмотреть информацию</button>';
        user += '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">';
        user += '   <span class="caret"></span>';
        user += '   <span class="sr-only">Toggle Dropdown</span>';
        user += ' </button>';
        user += '<ul class="dropdown-menu" role="menu">';
        user += ' <li><button type="button" class="delete-category" value="' + data.id + '">Удалить вписку</button></li>';
        user += '</ul>';
        user += ' </div> </td></tr>';

        return user;

    }

    //display modal form for creating new task
    $('#btn-add').click(function () {
        $('#btn-save').val("add");
        $('#frmTasks').trigger("reset");
        $('#myModal').modal('show');
    });



    $(".btn-save").click(function (e) {
        var lang = $(this).val();
        if (confirm("Подтвердите свое действие.")) {
            saveData(e,lang);
        }
    });





    function saveData(e,lang) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        e.preventDefault();

        var formData = {
            lang: lang,
            name: $('#name_' + lang).val(),
            parent_id:  $('#parent_category_' + lang).val(),
            group_id:  $('.category_group_id').val(),
            url_icon:  $('#thumbnail').val(),
        }
        console.log(formData);
        var my_url = url;
        my_url += '/edit-category';


        $.ajax({
            type: 'PUT',
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                console.log('Данные пришли: ', data);
                // if (state == "add") { //if user added a new record
                //     $('#tasks-list').append(user);
                // } else { //if user updated an existing record
                //
                //     $("#party_id" + data.id).replaceWith(formUser(data));
                // }

                document.location.reload(true);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }






});
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', 'MapController@getMap');
Route::get('', 'MapController@getMap');
Route::get('/markers/locale/{locale}', 'MapController@getAllMarkers');
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('admin', 'AdminController@index')->name('admin.home');


Route::group(['namespace' => 'Admin','middleware' => 'auth','prefix' =>'admin'], function() {
    Route::get('list-markers', 'Map\MarkerController@showListMarkers')->name('admin.list-markers');
    Route::get('add-marker', 'Map\MarkerController@formAddMarker')->name('admin.add-marker');
    Route::post('add-marker', 'Map\MarkerController@addMarker');
    Route::get('markers/{id}', 'Map\MarkerController@getMarker');
    Route::delete('markers/{id}', 'Map\MarkerController@deleteMarker');
    Route::put('markers/edit-marker', 'Map\MarkerController@editMarker');

    Route::get('list-categories', 'Map\CategoryController@showListCategories')->name('admin.list-categories');
    Route::get('add-category', 'Map\CategoryController@formAddCategory')->name('admin.add-category');
    Route::get('categories/{id}', 'Map\CategoryController@getCategory');
    Route::post('add-category', 'Map\CategoryController@addCategory');
    Route::put('categories/edit-category', 'Map\CategoryController@editCategory');
    Route::delete('categories/{id}', 'Map\CategoryController@deleteCategory');

    Route::get('list-languages', 'LanguageController@showListLanguages')->name('admin.list-languages');
    Route::get('add-language', 'LanguageController@formAddLanguage')->name('admin.add-language');
    Route::post('add-language', 'LanguageController@addLanguage');
    Route::delete('languages/{id}', 'LanguageController@deleteLanguage');

});
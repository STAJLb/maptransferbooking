<?php

namespace App\Http\Controllers\Admin\Map;

use App\Language;
use App\Marker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class MarkerController extends Controller
{
    public function showListMarkers(){
        $parentCategories = Category::where([
            'lang' => 'ru'
        ])->get();
        $categories = Category::nestable($parentCategories);
        return view('admin.map.markers.list-markers')->with([
            'markers' => Marker::where('lang','ru')->get(),
            'languages' => Language::all(),
            'categories' => $categories
        ]);
    }

    public function getMarker($id){

        return response()->json(Marker::where('group_id',$id)->get(), 200,array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);

    }



    public function formAddMarker(){
        $parentCategories = Category::where([
            'parent_id' => 0,
            'lang' => 'ru'
            ])->get();
        $categories = Category::nestable($parentCategories);
        return view('admin.map.markers.add-marker')->with([
            'categories' => $categories
        ]);
    }

    public function addMarker(Request $request){
         $rules = [
            'title_marker' => 'required',
            'description_marker' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
            'filepath_icons' => 'required',
        ];

        $messages = [
            'title_marker.required' => 'Поле Название объекта обязательно для заполнения.',
            'description_marker.required' => 'Поле Описание объекта обязательно для заполнения.',
            'longitude.required' => 'Поле Долгота обязательно для заполнения.',
            'latitude.required' => 'Поле Широта обязательно для заполнения.',
            'url_icon.required' => 'Поле Иконка обязательно для заполнения.',

        ];
        $this->validate($request, $rules, $messages);
           foreach (Language::all() as $language){
                $marker = new Marker();
                if(!strcasecmp($language->tag,'ru')){
                    $marker->name = $request->title_marker;
                     $marker->description = $request->description_marker;
                }else{
                     $marker->name = $language->tag;
                     $marker->description = $language->tag;
                }
               
                $marker->longitude = $request->longitude;
                $marker->latitude = $request->latitude;
                $marker->url_icon = $request->filepath_icons;
                $lastId = Marker::latest()->first();
                if($lastId){
                    $groupId = $lastId->group_id+1;
                }else{
                    $groupId = 1;
                }
                $marker->group_id = $groupId;
                $marker->lang = $language->tag;
                if(isset($request->categories)){
                     $marker->categories = implode($request->categories, ',');
                }
                $marker->save();
        }

        

        return redirect('admin/list-markers')->with(['message' => 'Маркер добавлен.']);
    }

    public function editMarker(Request $request)
    {

        if ($request->lang == 'ru') {

            $rules = [
                'name' => 'required',
                'description' => 'required',
                'longitude' => 'required',
                'latitude' => 'required',
                'url_icon' => 'required',
            ];

            $messages = [
                'name.required' => 'Поле Название объекта обязательно для заполнения.',
                'description.required' => 'Поле Описание объекта обязательно для заполнения.',
                'longitude.required' => 'Поле Долгота обязательно для заполнения.',
                'latitude.required' => 'Поле Широта обязательно для заполнения.',
                'url_icon.required' => 'Поле Иконка обязательно для заполнения.',

            ];
            $this->validate($request, $rules, $messages);

            $markers = Marker::where([
                'group_id' => $request->group_id,
            ])->get();
            foreach ($markers as $marker) {
                if($marker->lang == 'ru'){
                    $marker->name = $request->name;
                    $marker->description = $request->description;
                }
                $marker->longitude = $request->longitude;
                $marker->latitude = $request->latitude;
                $marker->url_icon = $request->url_icon;
                $marker->categories = NULL;
                if($request->categories){
                    foreach ($request->categories as $categoryId){
                        $categoryGroupId = Category::find(str_replace(',', '', $categoryId))->group_id;
                        $category = Category::where([
                            'lang'=> $marker->lang,
                            'group_id' => $categoryGroupId
                        ])->first();
                        if($marker->categories){
                            $marker->categories = $marker->categories.','.$category->id;
                        }else{
                            $marker->categories = $category->id;
                        }


                    }
                }else{
                    $marker->categories = NULL;
                }
                $marker->save();
            }
        }else{
            $marker = Marker::where([
                'lang' => $request->lang,
                'group_id' => $request->group_id
            ])->first();
            $marker->name = $request->name;
            $marker->description = $request->description;
            $marker->save();
        }



        $marker = Marker::where([
            'lang' => $request->lang,
            'group_id' => $request->group_id
        ])->get();


        return response()->json($marker, 200,array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
    }

    
    public function deleteMarker($id){
        Marker::where('group_id',$id)->delete();
        return response()->json(array('message' => 'Маркер удален.'), 200,array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
    }
}

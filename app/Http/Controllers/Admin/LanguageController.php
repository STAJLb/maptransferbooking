<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Language;
use App\Http\Controllers\Controller;
use App\Marker;
use Illuminate\Http\Request;

class LanguageController extends Controller{

    public function showListLanguages(){
        return view('admin.languages.list-languages')->with([
            'languages' => Language::all()
        ]);
    }

    public function formAddLanguage()
    {
        return view('admin.languages.add-language');
    }

    public function addLanguage(Request $request)
    {

        $rules = [
            'tag' => 'required|unique:languages|max:10|min:1',
            'name' => 'required|unique:languages|max:30|min:1',
        ];

        $messages = [
            'name.unique' => 'Такое Название языка уже существует.',
            'name.required' => 'Поле Название языка обязательно для заполнения.',
            'name.max' => 'Поле Название языка должно иметь максимальный размер 4 символа.',
            'name.min' => 'Поле Название языка должно иметь минимальный размер 1 символ.',
        ];
        $this->validate($request, $rules, $messages);
        $language = new Language();
        $language->name = $request->name;
        $language->tag = $request->tag;
        $language->save();


        $parentCategories = Category::where([
            'parent_id' => 0
        ])->get();
        $existsParentCategories = array();
        //Перебираем родительские категории
        foreach ($parentCategories as $parentCategory){
                if(!in_array($parentCategory->group_id,$existsParentCategories)){
                    $newCategory = new Category();
                    $newCategory->name = $request->tag;
                    $newCategory->parent_id = 0;
                    $newCategory->group_id = $parentCategory->group_id;
                    $newCategory->lang = $request->tag;
                    $newCategory->url_icon = $parentCategory->url_icon;
                    $newCategory->save();
                    array_push($existsParentCategories,$parentCategory->group_id);
                }

        }
        $existsChildrenCategories = array();
        //Перебираем дочерние категории
        $childrenCategories = Category::where('parent_id','<>',0)->get();
        foreach ($childrenCategories as $childrenCategory){
            if(!in_array($childrenCategory->group_id,$existsChildrenCategories)) {
                $newCategory = new Category();
                $newCategory->name = $request->tag;
                $parentCategoryGroupId = Category::find($childrenCategory->parent_id)->group_id;
                $parentCategory = Category::where([
                    'lang' => $request->name,
                    'group_id' => $parentCategoryGroupId
                ])->first();
                if($parentCategory){
                    $newCategory->parent_id = $parentCategory->id;
                }

                $newCategory->group_id = $childrenCategory->group_id;
                $newCategory->lang = $request->tag;
                $newCategory->url_icon = $childrenCategory->url_icon;
                $newCategory->save();
                array_push($existsChildrenCategories,$childrenCategory->group_id);
            }


        }

        $markers = Marker::all();
        $arrayGroupId = array();
        foreach ($markers as $marker){
            if(!in_array($marker->group_id,$arrayGroupId)){
                $newMarker = new Marker();
                $newMarker->name = $request->tag;
                $newMarker->description = $request->tag;
                $newMarker->longitude = $marker->longitude;
                $newMarker->latitude = $marker->latitude;
                $newMarker->url_icon = $marker->url_icon;
                $categoriesId = explode(',',$marker->categories);
                if(count($categoriesId) > 1){
                foreach($categoriesId as $categoryId){
                    $categoryGroupId = Category::find($categoryId)->group_id;
                    $category = Category::where([
                        'lang' => $request->tag,
                        'group_id' => $categoryGroupId
                        ])->first();
                        
                    $newMarker->categories = $newMarker->categories.$category->id.',';
                }
                }else{
                     $newMarker->categories = $marker->categories;
                }

                $newMarker->group_id = $marker->group_id;
                $newMarker->lang = $request->tag;
                $newMarker->save();
                array_push($arrayGroupId,$marker->group_id);
            }
        }
        

        return redirect('admin/list-languages')->with(['message' => 'Язык добавлен.']);
    }

    public function deleteLanguage($id){
        $nameLanguage = Language::find($id)->tag;
        Language::find($id)->delete();
        Category::where('lang',$nameLanguage)->delete();
        Marker::where('lang',$nameLanguage)->delete();

        return response()->json(array('message' =>'Язык удален'),  200,array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
    }
}
